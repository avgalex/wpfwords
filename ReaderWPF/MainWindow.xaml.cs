﻿namespace ReaderWPF
{
    using System.Windows;
    using System.Windows.Input;

    using ReaderWPF.Models.Provider;
    using ReaderWPF.ViewModels;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.ViewModel = new ReaderViewModel(new Provider());
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        /// <summary>
        /// </summary>
        public ReaderViewModel ViewModel { get; private set; }

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.ViewModel.Model;
            var commandModel = this.ViewModel.ReadCommandModel;

            this.btRead.Command = commandModel.Command;
            this.btRead.CommandParameter = this.DataContext;
            var commandBinding = new CommandBinding(
                commandModel.Command, 
                commandModel.OnExecute, 
                commandModel.OnCanExecute);

            this.btRead.CommandBindings.Add(commandBinding);

            this.ViewModel.ShowLogin();
        }

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        private void BtReadClick(object sender, RoutedEventArgs e)
        {
            this.CustomComboBox.SelectedIndex = -1;
        }
    }
}