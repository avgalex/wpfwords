﻿namespace ReaderWPF.Models
{
    using System.Collections.ObjectModel;

    using ReaderWPF.Models.Provider;

    using WpfFormsLib.Models;

    /// <summary>
    /// </summary>
    public class ReaderModel : BaseModel
    {
        /// <summary>
        /// </summary>
        private IProvider provider;

        /// <summary>
        /// </summary>
        private ObservableCollection<string> words;

        /// <summary>
        /// </summary>
        /// <param name="provider">
        /// </param>
        public ReaderModel(IProvider provider)
        {
            this.provider = provider;
        }

        /// <summary>
        /// </summary>
        public ObservableCollection<string> Words
        {
            get
            {
                return this.words;
            }

            set
            {
                this.words = value;
                this.Notify("Words");
            }
        }

        /// <summary>
        /// </summary>
        public void ReadWords()
        {
            this.provider.ReadWords(this);
        }
    }
}