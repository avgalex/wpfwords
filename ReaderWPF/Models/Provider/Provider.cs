﻿

namespace ReaderWPF.Models.Provider
{
    using System;
    using System.Collections.ObjectModel;

    using ActionService;

    /// <summary>
    /// The provider.
    /// </summary>
    public class Provider : IProvider
    {
        /// <summary>
        /// </summary>
        private IService service = new Service();

        /// <summary>
        /// </summary>
        public Provider()
        {
            this.service.Disconnect += this.ServerDisconnect;
        }

        /// <summary>
        /// The disconnect.
        /// </summary>
        public event Action Disconnect;

        /// <summary>
        /// </summary>
        /// <param name="model">
        /// </param>
        public void ReadWords(ReaderModel model)
        {
            var list = this.service.ReadWords();

            var maxCount = 500;
            if (list.Count == maxCount)
            {
                list[maxCount - 1] = "...";
            }

            model.Words = new ObservableCollection<string>(list);
        }

        /// <summary>
        /// </summary>
        protected virtual void OnDisconnect()
        {
            var handler = this.Disconnect;
            if (handler != null)
            {
                handler();
            }
        }

        /// <summary>
        /// </summary>
        private void ServerDisconnect()
        {
            this.OnDisconnect();
        }
    }
}