﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IProvider.cs" company="">
//   
// </copyright>
// <summary>
//   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ReaderWPF.Models.Provider
{
    using System;

    /// <summary>
    /// </summary>
    public interface IProvider
    {
        /// <summary>
        /// </summary>
        event Action Disconnect;

        /// <summary>
        /// </summary>
        /// <param name="model">
        /// </param>
        void ReadWords(ReaderModel model);
    }
}