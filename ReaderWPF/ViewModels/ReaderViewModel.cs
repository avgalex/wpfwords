﻿namespace ReaderWPF.ViewModels
{
    using ReaderWPF.Commands;
    using ReaderWPF.Models;
    using ReaderWPF.Models.Provider;

    using WpfFormsLib.ViewModels;

    /// <summary>
    /// The reader view model.
    /// </summary>
    public class ReaderViewModel : ViewModelBase
    {
        /// <summary>
        /// The model.
        /// </summary>
        private ReaderModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderViewModel"/> class.
        /// </summary>
        public ReaderViewModel()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ReaderViewModel"/> class.
        /// </summary>
        /// <param name="provider">
        /// The provider.
        /// </param>
        public ReaderViewModel(IProvider provider)
        {
            this.model = new ReaderModel(provider);
            this.ReadCommandModel = new ReadCommand();
            provider.Disconnect += this.ServerDisconnect;
        }

        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        public ReaderModel Model
        {
            get
            {
                return this.model;
            }

            set
            {
                if (this.model == value)
                {
                    return;
                }

                this.model = value;
                this.OnPropertyChanged("WordsModel");
            }
        }

        /// <summary>
        /// Gets the read command model.
        /// </summary>
        public CommandModel ReadCommandModel { get; private set; }
    }
}