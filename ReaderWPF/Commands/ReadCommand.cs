﻿namespace ReaderWPF.Commands
{
    using System;
    using System.Windows;
    using System.Windows.Input;

    using ReaderWPF.Models;

    using WpfFormsLib.FormWait;
    using WpfFormsLib.ViewModels;

    /// <summary>
    /// </summary>
    public class ReadCommand : CommandModel
    {
        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        public override void OnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var readerModel = e.Parameter as ReaderModel;
            if (readerModel == null)
            {
                return;
            }

            var w = new WindowWaitHandler();
            try
            {
                w.Start();
                readerModel.ReadWords();
                w.Stop();
            }
            catch (Exception)
            {
                w.Stop();
                MessageBox.Show("Обновление не выполнено по неизвестной причине");
            }
        }
    }
}