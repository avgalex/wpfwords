﻿namespace ReaderWPF.CustomCombobox
{
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// The combobox.
    /// </summary>
    public static class Combobox
    {
        /// <summary>
        /// The get is nullable.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static bool GetIsNullable(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsNullableProperty);
        }

        /// <summary>
        /// The set is nullable.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public static void SetIsNullable(DependencyObject obj, bool value)
        {
            obj.SetValue(IsNullableProperty, value);
        }

        /// <summary>
        /// The on is nullable changed.
        /// </summary>
        /// <param name="d">
        /// The d.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void OnIsNullableChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                var comboBox = (ComboBox)d;
                comboBox.SizeChanged += comboBox_SizeChanged;
            }
        }

        /// <summary>
        /// The combo box_ size changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void comboBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var comboBox = (ComboBox)sender;
            comboBox.SizeChanged -= comboBox_SizeChanged;

            ApplyIsNullable(comboBox);

            // also apply after the selection has changed
            comboBox.SelectionChanged += delegate { ApplyIsNullable(comboBox); };
        }

        /// <summary>
        /// The apply is nullable.
        /// </summary>
        /// <param name="comboBox">
        /// The combo box.
        /// </param>
        private static void ApplyIsNullable(ComboBox comboBox)
        {
            var isNullable = GetIsNullable(comboBox);
            var clearButton = (Button)GetClearButton(comboBox);
            if (clearButton != null)
            {
                clearButton.Click -= clearButton_Click;
                clearButton.Click += clearButton_Click;

                if (isNullable && comboBox.SelectedIndex != -1)
                {
                    clearButton.Visibility = Visibility.Visible;
                }
                else
                {
                    clearButton.Visibility = Visibility.Collapsed;
                }
            }
        }

        /// <summary>
        /// The clear button_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private static void clearButton_Click(object sender, RoutedEventArgs e)
        {
            var clearButton = (Button)sender;
            var parent = VisualTreeHelper.GetParent(clearButton);

            while (!(parent is ComboBox))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            var comboBox = (ComboBox)parent;

            // clear the selection
            comboBox.SelectedIndex = -1;
        }

        /// <summary>
        /// The get clear button.
        /// </summary>
        /// <param name="reference">
        /// The reference.
        /// </param>
        /// <returns>
        /// The <see cref="Button"/>.
        /// </returns>
        private static Button GetClearButton(DependencyObject reference)
        {
            for (int childIndex = 0; childIndex < VisualTreeHelper.GetChildrenCount(reference); childIndex++)
            {
                var child = VisualTreeHelper.GetChild(reference, childIndex);

                if (child is Button && ((Button)child).Name == "PART_ClearButton")
                {
                    return (Button)child;
                }

                var clearButton = GetClearButton(child);
                if (clearButton is Button)
                {
                    return clearButton;
                }
            }

            return null;
        }

        // Using a DependencyProperty as the backing store for IsNullable.  This enables animation, styling, binding, etc...
        /// <summary>
        /// The is nullable property.
        /// </summary>
        public static readonly DependencyProperty IsNullableProperty = DependencyProperty.RegisterAttached(
            "IsNullable", 
            typeof(bool), 
            typeof(Combobox), 
            new UIPropertyMetadata(false, OnIsNullableChanged));
    }
}