﻿namespace WriterWPF
{
    using System.Windows;
    using System.Windows.Input;

    using WriterWPF.Models.Provider;
    using WriterWPF.ViewModels;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.ViewModel = new WriterViewModel(new Provider());
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        /// <summary>
        /// Gets the view model.
        /// </summary>
        public WriterViewModel ViewModel { get; private set; }

        /// <summary>
        /// The button_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// The preview text input handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PreviewTextInputHandler(object sender, TextCompositionEventArgs e)
        {
            TextBoxMask.PreviewTextInputHandler(sender, e);
        }

        /// <summary>
        /// The pasting handler.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            TextBoxMask.PastingHandler(sender, e);
        }

        /// <summary>
        /// The window_ loaded.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = this.ViewModel.Model;
            var commandModel = this.ViewModel.UpdateCommandModel;

            this.btUpdate.Command = commandModel.Command;
            this.btUpdate.CommandParameter = this.DataContext;
            var commandBinding = new CommandBinding(
                commandModel.Command, 
                commandModel.OnExecute, 
                commandModel.OnCanExecute);
            this.btUpdate.CommandBindings.Add(commandBinding);

            this.ViewModel.ShowLogin();
        }
    }
}