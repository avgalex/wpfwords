﻿namespace WriterWPF.ViewModels
{
    using WpfFormsLib.ViewModels;

    using WriterWPF.Commands;
    using WriterWPF.Models;
    using WriterWPF.Models.Provider;

    /// <summary>
    /// </summary>
    public class WriterViewModel : ViewModelBase
    {
        /// <summary>
        /// </summary>
        private WriterModel model;

        /// <summary>
        /// </summary>
        /// <param name="provider">
        /// </param>
        public WriterViewModel(IProvider provider)
        {
            this.model = new WriterModel(provider);
            this.UpdateCommandModel = new UpdateCommand();
            provider.Disconnect += this.ServerDisconnect;
        }

        /// <summary>
        /// </summary>
        public WriterModel Model
        {
            get
            {
                return this.model;
            }

            set
            {
                if (this.model == value)
                {
                    return;
                }

                this.model = value;
                this.OnPropertyChanged("WordsModel");
            }
        }

        /// <summary>
        /// </summary>
        public CommandModel UpdateCommandModel { get; private set; }
    }
}