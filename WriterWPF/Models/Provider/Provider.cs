﻿namespace WriterWPF.Models.Provider
{
    using System;

    using ActionService;

    /// <summary>
    /// </summary>
    public class Provider : IProvider
    {
        /// <summary>
        /// </summary>
        private IService service = new Service();

        /// <summary>
        /// </summary>
        public Provider()
        {
            this.service.Disconnect += this.ServerDisconnect;
        }

        /// <summary>
        /// </summary>
        /// <param name="writerModel">
        /// </param>
        /// <exception cref="InvalidCastException">
        /// </exception>
        public void UpdateWords(WriterModel writerModel)
        {
            int count;
            if (!int.TryParse(writerModel.Count, out count))
            {
                throw new InvalidCastException();
            }

            this.service.UpdateWords(count);
        }

        /// <summary>
        /// </summary>
        public event Action Disconnect;

        /// <summary>
        /// </summary>
        protected virtual void OnDisconnect()
        {
            var handler = this.Disconnect;
            if (handler != null)
            {
                handler();
            }
        }

        /// <summary>
        /// </summary>
        private void ServerDisconnect()
        {
            this.OnDisconnect();
        }
    }
}