﻿namespace WriterWPF.Models.Provider
{
    using System;

    /// <summary>
    /// </summary>
    public interface IProvider
    {
        /// <summary>
        /// </summary>
        /// <param name="writerModel">
        /// </param>
        void UpdateWords(WriterModel writerModel);

        /// <summary>
        /// </summary>
        event Action Disconnect;
    }
}