﻿namespace WriterWPF.Models
{
    using WpfFormsLib.Models;

    using WriterWPF.Models.Provider;

    /// <summary>
    /// </summary>
    public class WriterModel : BaseModel
    {
        /// <summary>
        /// </summary>
        private IProvider provider;

        /// <summary>
        /// </summary>
        /// <param name="provider">
        /// </param>
        public WriterModel(IProvider provider)
        {
            this.provider = provider;
        }

        /// <summary>
        /// </summary>
        public string Count { get; set; }

        /// <summary>
        /// </summary>
        public void UpdateWords()
        {
            this.provider.UpdateWords(this);
        }
    }
}