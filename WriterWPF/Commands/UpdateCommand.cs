﻿namespace WriterWPF.Commands
{
    using System;
    using System.Windows;
    using System.Windows.Input;

    using WpfFormsLib.FormWait;
    using WpfFormsLib.ViewModels;

    using WriterWPF.Models;

    /// <summary>
    /// </summary>
    internal class UpdateCommand : CommandModel
    {
        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        public override void OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            var writerModel = e.Parameter as WriterModel;

            if (writerModel != null)
            {
                e.CanExecute = !string.IsNullOrEmpty(writerModel.Count);
            }

            e.Handled = true;
        }

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        public override void OnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var writerModel = e.Parameter as WriterModel;
            if (writerModel == null)
            {
                return;
            }

            var w = new WindowWaitHandler();
            try
            {
                w.Start();
                writerModel.UpdateWords();
                w.Stop();
            }
            catch (Exception)
            {
                w.Stop();
                MessageBox.Show("Обновление не выполнено по неизвестной причине");
            }
        }
    }
}