﻿namespace WriterWPF
{
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;

    /// <summary>
    /// </summary>
    public static class TextBoxMask
    {
        /// <summary>
        /// </summary>
        private const int minValue = 0;

        /// <summary>
        /// </summary>
        private const int maxValue = 1000000;

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        public static void PreviewTextInputHandler(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            if (textBox == null)
            {
                return;
            }

            bool isValid = IsSymbolValid(e.Text);

            e.Handled = !isValid;

            if (!isValid)
            {
                return;
            }

            int caret = textBox.CaretIndex;
            string text = textBox.Text;

            int selectionLength = 0;

            var s = (caret < textBox.Text.Length) ? text.Substring(caret) : string.Empty;
            text = string.Format("{0}{1}{2}", text.Substring(0, caret), e.Text, s);
            caret++;

            int val;
            if (int.TryParse(text, out val))
            {
                int newVal = ValidateLimits(minValue, maxValue, val);

                if (val != newVal)
                {
                    text = newVal.ToString();
                }
            }
            else
            {
                text = "0";
            }

            while (text.Length > 1 && text[0] == '0')
            {
                text = text.Substring(1);
                if (caret > 0)
                {
                    caret--;
                }
            }

            if (caret > text.Length)
            {
                caret = text.Length;
            }

            textBox.Text = text;
            textBox.CaretIndex = caret;
            textBox.SelectionStart = caret;
            textBox.SelectionLength = selectionLength;
            e.Handled = true;
        }

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        public static void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            var textBox = sender as TextBox;

            var clipboard = e.DataObject.GetData(typeof(string)) as string;
            clipboard = ValidateValue(clipboard);

            if (!string.IsNullOrEmpty(clipboard))
            {
                if (textBox != null)
                {
                    textBox.Text = clipboard;
                }
            }

            e.CancelCommand();
            e.Handled = true;
        }

        /// <summary>
        /// </summary>
        /// <param name="str">
        /// </param>
        /// <returns>
        /// </returns>
        private static bool IsSymbolValid(string str)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(str);
        }

        /// <summary>
        /// </summary>
        /// <param name="min">
        /// </param>
        /// <param name="max">
        /// </param>
        /// <param name="value">
        /// </param>
        /// <returns>
        /// </returns>
        private static int ValidateLimits(int min, int max, int value)
        {
            if (value < min)
            {
                return min;
            }

            if (value > max)
            {
                return max;
            }

            return value;
        }

        /// <summary>
        /// </summary>
        /// <param name="value">
        /// </param>
        /// <returns>
        /// </returns>
        private static string ValidateValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            value = value.Trim();
            int result;
            return int.TryParse(value, out result) ? value : string.Empty;
        }
    }
}