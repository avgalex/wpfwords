﻿namespace ActionService
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// </summary>
        /// <param name="count">
        /// </param>
        void UpdateWords(int count);

        /// <summary>
        /// </summary>
        event Action Disconnect;

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        IList<string> ReadWords();
    }
}