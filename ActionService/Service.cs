﻿namespace ActionService
{
    using System;
    using System.Collections.Generic;

    using ActionService.Randomize;

    using DataObject;
    using DataObject.DAO;

    /// <summary>
    /// The service.
    /// </summary>
    public class Service : IService
    {
        /// <summary>
        /// </summary>
        private static readonly IWordsDao wordsDao = new WordsDao();

        /// <summary>
        /// </summary>
        public Service()
        {
            ServerManager.GetServerManager().Disconnect += this.ServiceDisconnect;
        }

        /// <summary>
        /// The update words.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        public void UpdateWords(int count)
        {
            var newWords = this.GetWords(count);
            wordsDao.UpdateWords(newWords);
        }

        /// <summary>
        /// </summary>
        public event Action Disconnect;

        /// <summary>
        /// </summary>
        /// <returns>
        /// </returns>
        public IList<string> ReadWords()
        {
            int maxCount = 500;

            IList<string> result = wordsDao.GetWords(maxCount);
            return result;
        }

        /// <summary>
        /// </summary>
        protected virtual void OnDisconnect()
        {
            var handler = this.Disconnect;
            if (handler != null)
            {
                handler();
            }
        }

        /// <summary>
        /// </summary>
        private void ServiceDisconnect()
        {
            this.OnDisconnect();
        }

        /// <summary>
        /// </summary>
        /// <param name="count">
        /// </param>
        /// <returns>
        /// </returns>
        private IList<string> GetWords(int count)
        {
            var result = new List<string>();

            if (count >= 1)
            {
                var firstRow = DateTime.Today.ToLongDateString();
                result.Add(firstRow);
            }

            if (count >= 2)
            {
                var secondRow = DateTime.Now.ToLongTimeString();
                result.Add(secondRow);
            }

            if (count >= 3)
            {
                var threeRow = count.ToString();
                result.Add(threeRow);
            }

            if (count >= 4)
            {
                this.AddRandomString(count, result);
            }

            return result;
        }

        /// <summary>
        /// The add random string.
        /// </summary>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <param name="result">
        /// The result.
        /// </param>
        private void AddRandomString(int count, ICollection<string> result)
        {
            var strCount = count - 3;

            for (int i = 0; i < strCount; i++)
            {
                var s = this.GenerateString();
                result.Add(s);
            }
        }

        /// <summary>
        /// The generate string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GenerateString()
        {
            var length = RandomStringGenerator.RandomLength();
            var result = RandomStringGenerator.RandomString(length);
            return result;
        }
    }
}