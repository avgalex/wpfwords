﻿namespace ActionService.Randomize
{
    using System;
    using System.Text;

    public class RandomStringGenerator
    {
        private static Random random = RandomProvider.GetThreadRandom();

        public static int RandomLength()
        {
            return random.Next(5, 31);
        }

        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            var ch = (char)random.Next('A', 'Z' + 1);
         
            builder.Append(ch);

            for (int i = 1; i < size; i++)
            {
                ch = (char)random.Next('a', 'z' + 1);
                builder.Append(ch);
            }

            return builder.ToString();
        } 
    }
}