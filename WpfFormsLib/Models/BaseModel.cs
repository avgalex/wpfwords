﻿namespace WpfFormsLib.Models
{
    using System.ComponentModel;
    using System.Windows.Threading;

    /// <summary>
    /// The base model.
    /// </summary>
    public abstract class BaseModel : INotifyPropertyChanged
    {
       /// <summary>
        /// The property changed event.
        /// </summary>
        private PropertyChangedEventHandler propertyChangedEvent;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseModel"/> class.
        /// </summary>
        public BaseModel()
        {

        }

        /// <summary>
        /// The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                this.propertyChangedEvent += value;
            }

            remove
            {
                this.propertyChangedEvent -= value;
            }
        }

        /// <summary>
        /// The notify.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        protected void Notify(string propertyName)
        {
            if (this.propertyChangedEvent != null)
            {
                this.propertyChangedEvent(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}