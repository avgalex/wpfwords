﻿namespace WpfFormsLib.FormInput
{
    using DataObject;

    /// <summary>
    /// </summary>
    internal class Provider
    {
        /// <summary>
        /// </summary>
        private ServerManager serverManager = ServerManager.GetServerManager();

        /// <summary>
        /// </summary>
        /// <param name="serverAddress">
        /// </param>
        /// <param name="login">
        /// </param>
        /// <param name="password">
        /// </param>
        public void Login(string serverAddress, string login, string password)
        {
            this.serverManager.Connect(serverAddress, login, password);
        }
    }
}