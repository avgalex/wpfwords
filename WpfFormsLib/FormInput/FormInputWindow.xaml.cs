﻿namespace WpfFormsLib.FormInput
{
    using System;
    using System.Data.SqlClient;
    using System.Windows;

    using WpfFormsLib.FormWait;

    /// <summary>
    /// Interaction logic for FormInputWindow.xaml
    /// </summary>
    public partial class FormInputWindow : Window
    {
        /// <summary>
        /// The logi failed.
        /// </summary>
        private const int LogiFailed = 18456;

        /// <summary>
        /// Initializes a new instance of the <see cref="FormInputWindow"/> class.
        /// </summary>
        public FormInputWindow()
        {
            this.InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
        }

        /// <summary>
        /// The bt connect click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtConnectClick(object sender, RoutedEventArgs e)
        {
            // this.Hide();
            var w = new WindowWaitHandler();
            w.Start();

            try
            {
                this.ConnectToServer();

                w.Stop();
                this.Close();
            }
            catch (Exception ex)
            {
                w.Stop();
                this.ExceptionHandler(ex);

                // this.ShowDialog();
            }
        }

        /// <summary>
        /// The exception handler.
        /// </summary>
        /// <param name="ex">
        /// The ex.
        /// </param>
        private void ExceptionHandler(Exception ex)
        {
            var innerException = ex.InnerException;

            if (innerException is SqlException)
            {
                var sqlEx = innerException as SqlException;
                if (sqlEx.Number == LogiFailed)
                {
                    MessageBox.Show(@"Неверный логин или пароль!");
                    return;
                }
            }

            MessageBox.Show(@"Подключение не выполнено по неизвестной причине");
        }

        /// <summary>
        /// The connect to server.
        /// </summary>
        private void ConnectToServer()
        {
            string serverAddress = this.txtAddress.Text.Trim();
            string login = this.txtLogin.Text.Trim();
            string password = this.txtPassword.Password.Trim();

            var provider = new Provider();
            provider.Login(serverAddress, login, password);
        }
    }
}