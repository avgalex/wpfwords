﻿namespace WpfFormsLib.FormWait
{
    #region

    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Interop;

    #endregion

    /// <summary>
    ///     Interaction logic for FormInputWindow.xaml
    /// </summary>
    public partial class FormWaitWindow : Window
    {
        /// <summary>
        ///     The gw l_ style.
        /// </summary>
        private const int GWL_STYLE = -16;

        /// <summary>
        ///     The w s_ sysmenu.
        /// </summary>
        private const int WS_SYSMENU = 0x80000;

        /// <summary>
        /// The alt f 4 pressed.
        /// </summary>
        private bool altF4Pressed;

        /// <summary>
        ///     Initializes a new instance of the <see cref="FormWaitWindow" /> class.
        /// </summary>
        public FormWaitWindow()
        {
            this.InitializeComponent();
            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.ShowInTaskbar = false;
        }

        /// <summary>
        /// Sets a value indicating whether alt f 4 pressed.
        /// </summary>
        public bool AltF4Pressed
        {
            set
            {
                this.altF4Pressed = value;
            }
        }

        /// <summary>
        /// The get window long.
        /// </summary>
        /// <param name="hWnd">
        /// The h wnd.
        /// </param>
        /// <param name="nIndex">
        /// The n index.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        /// <summary>
        /// The set window long.
        /// </summary>
        /// <param name="hWnd">
        /// The h wnd.
        /// </param>
        /// <param name="nIndex">
        /// The n index.
        /// </param>
        /// <param name="dwNewLong">
        /// The dw new long.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        /// <summary>
        /// The window_ loaded.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            var windowInteropHelper = new WindowInteropHelper(this);
            var hwnd = windowInteropHelper.Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }

        /// <summary>
        /// The window_ closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void WindowClosing(object sender, CancelEventArgs e)
        {
            if (!this.altF4Pressed)
            {
                return;
            }

            e.Cancel = true;
            this.altF4Pressed = false;
        }

        /// <summary>
        /// The window key down.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void WindowKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers != ModifierKeys.Alt || e.SystemKey != Key.F4)
            {
                return;
            }

            e.Handled = true;
            this.altF4Pressed = true;
        }

        /// <summary>
        /// The bt cancel click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtCancelClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(@"Невозможно отменить этот процесс в данный момент. Пожайлуста подождите");
        }
    }
}