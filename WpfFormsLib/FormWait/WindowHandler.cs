﻿namespace WpfFormsLib.FormWait
{
    using System;
    using System.Threading;
    using System.Windows.Threading;

    /// <summary>
    /// The window wait handler.
    /// </summary>
    public class WindowWaitHandler
    {
        /// <summary>
        /// The popup.
        /// </summary>
        private FormWaitWindow Popup;

        /// <summary>
        /// The status thread.
        /// </summary>
        private Thread StatusThread;

        /// <summary>
        /// The start.
        /// </summary>
        /// <param name="owner"></param>
        public void Start()
        {
            this.StatusThread = new Thread(
                () =>
                    {
                        try
                        {
                            this.Popup = new FormWaitWindow();
                           
                            this.Popup.Closed += (lsender, le) =>
                                {
                                   this.Closed();
                                };

                            this.Popup.ShowDialog();

                            Dispatcher.Run();
                        }
                        catch (Exception ex)
                        {
                        }
                    });

            this.StatusThread.SetApartmentState(ApartmentState.STA);
            this.StatusThread.Priority = ThreadPriority.Normal;
            this.StatusThread.Start();
        }

        private void Closed()
        {
            this.Popup.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Background);
            this.Popup = null;
            this.StatusThread = null;
        }

        /// <summary>
        /// The stop.
        /// </summary>
        public void Stop()
        {
            Thread.Sleep(100);

            if (this.Popup != null)
            {
                this.Popup.Dispatcher.BeginInvoke(
                    new Action(
                        () =>
                            {
                                this.Popup.AltF4Pressed = false;
                                this.Popup.Close();
                            }));
            }

         
        }
    }
}