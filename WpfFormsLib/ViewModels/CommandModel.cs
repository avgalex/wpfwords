﻿namespace WpfFormsLib.ViewModels
{
    using System.Windows.Input;

    /// <summary>
    /// </summary>
    public abstract class CommandModel
    {
        /// <summary>
        /// </summary>
        public CommandModel()
        {
            this.Command = new RoutedUICommand();
        }

        /// <summary>
        /// </summary>
        public RoutedUICommand Command { get; private set; }

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        public abstract void OnExecute(object sender, ExecutedRoutedEventArgs e);

        /// <summary>
        /// </summary>
        /// <param name="sender">
        /// </param>
        /// <param name="e">
        /// </param>
        public virtual void OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
            e.Handled = true;
        }
    }
}