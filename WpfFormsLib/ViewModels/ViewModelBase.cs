﻿namespace WpfFormsLib.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.Windows.Threading;

    using WpfFormsLib.FormInput;

    /// <summary>
    /// The view model base.
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// </summary>
        protected Dispatcher _dispatcher;

        /// <summary>
        /// </summary>
        protected volatile bool isLogiState;

        /// <summary>
        /// </summary>
        public ViewModelBase()
        {
            this._dispatcher = Dispatcher.CurrentDispatcher;
        }

        /// <summary>
        /// The property changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// The show login.
        /// </summary>
        public virtual void ShowLogin()
        {
            var form = new FormInputWindow();
            form.ShowDialog();
        }

        /// <summary>
        /// The on property changed.
        /// </summary>
        /// <param name="propertyName">
        /// The property name.
        /// </param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// The server disconnect.
        /// </summary>
        protected void ServerDisconnect()
        {
            if (this.isLogiState)
            {
                return;
            }

            this.isLogiState = true;
            Action action = () =>
                {
                    this.ShowLogin();
                    this.isLogiState = false;
                };

            this._dispatcher.BeginInvoke(action);
        }
    }
}