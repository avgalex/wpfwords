﻿namespace DataObject.DAO
{
    using System.Collections.Generic;

    /// <summary>
    /// </summary>
    public interface IWordsDao
    {
        /// <summary>
        /// </summary>
        /// <param name="wordsList">
        /// </param>
        void UpdateWords(IList<string> wordsList);

        /// <summary>
        /// </summary>
        /// <param name="maxCount">
        /// </param>
        /// <returns>
        /// </returns>
        IList<string> GetWords(int maxCount);
    }
}