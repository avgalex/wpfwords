﻿namespace DataObject.DAO
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    #endregion

    /// <summary>
    /// </summary>
    public class WordsDao : IWordsDao
    {
        /// <summary>
        /// </summary>
        private static Func<IDataReader, string> Make = reader => reader["Word"].ToString();

        /// <summary>
        /// </summary>
        private ServerManager manager = ServerManager.GetServerManager();

        /// <summary>
        /// </summary>
        /// <param name="wordsList">
        /// </param>
        public void UpdateWords(IList<string> wordsList)
        {
            this.CheckTableWords();
            this.TruncateTableWords();
            this.InsertDataToTableWords(wordsList);
        }

        /// <summary>
        /// </summary>
        /// <param name="maxCount">
        /// </param>
        /// <returns>
        /// </returns>
        public IList<string> GetWords(int maxCount)
        {
            var q = "SELECT TOP 500 [Word] FROM WORDS ORDER BY [ID]";
            var result = this.manager.Read(q, Make).ToList();
            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name="wordsList">
        /// </param>
        private void InsertDataToTableWords(IList<string> wordsList)
        {
            DataTable dt = new DataTable();
            var dataColumn = new DataColumn("Word", typeof(string));
            dt.Columns.Add(dataColumn);

            foreach (var w in wordsList)
            {
                var row = dt.NewRow();
                row.ItemArray = new[] { w };
                dt.Rows.Add(row);
            }

            this.manager.BulkInsert(dt);
        }

        /// <summary>
        /// </summary>
        private void TruncateTableWords()
        {
            this.manager.Truncate();
        }

        /// <summary>
        /// </summary>
        private void CheckTableWords()
        {
            this.manager.CheckTableWords();
        }
    }
}