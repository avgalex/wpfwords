﻿namespace DataObject
{
    #region

    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Timers;

    using Microsoft.SqlServer.Management.Common;
    using Microsoft.SqlServer.Management.Smo;

    #endregion

    /// <summary>
    ///     The server manager.
    /// </summary>
    public class ServerManager
    {
        /// <summary>
        ///     The table name.
        /// </summary>
        private const string TableName = "Words";

        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly ServerManager instance = new ServerManager();

        /// <summary>
        ///     The server connection.
        /// </summary>
        private ServerConnection serverConnection;

        /// <summary>
        ///     The server.
        /// </summary>
        private Server server;

        /// <summary>
        ///     The db name.
        /// </summary>
        private const string DBName = "Test";

        /// <summary>
        ///     The get server manager.
        /// </summary>
        /// <returns>
        ///     The <see cref="ServerManager" />.
        /// </returns>
        public static ServerManager GetServerManager()
        {
            return instance;
        }

        /// <summary>
        ///     Gets the connection string.
        /// </summary>
        public string ConnectionString
        {
            get
            {
                var server1 = this.server;
                if (server1 != null)
                {
                    return server1.ConnectionContext.ConnectionString;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// The connect.
        /// </summary>
        /// <param name="serverName">
        /// The server name.
        /// </param>
        /// <param name="login">
        /// The login.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        public void Connect(string serverName, string login, string password)
        {
            this.serverConnection = new ServerConnection
                                        {
                                            ServerInstance = serverName, 
                                            LoginSecure = false, 
                                            Login = login, 
                                            Password = password
                                        };

            this.server = new Server(this.serverConnection);
            this.server.ConnectionContext.Connect();

            this.StartHeartBeat();
        }

        /// <summary>
        ///     The disconnect.
        /// </summary>
        public event Action Disconnect;

        /// <summary>
        ///     The start heart beat.
        /// </summary>
        private void StartHeartBeat()
        {
            var timer = new Timer(1000); // Set up the timer for 3 seconds

            // Type "_timer.Elapsed += " and press tab twice.
            timer.Elapsed += this.timer_Elapsed;
            timer.Enabled = true;
        }

        /// <summary>
        /// The timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                var query = "select 1";
                this.Scalar(query);
            }
            catch (Exception exception)
            {
                this.OnDisconnect();
            }
        }

        /// <summary>
        /// The scalar.
        /// </summary>
        /// <param name="sql">
        /// The sql.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Scalar(string sql)
        {
            using (var connection = this.CreateConnection())
            {
                using (var command = this.CreateCommand(sql, connection))
                {
                    return command.ExecuteScalar();
                }
            }
        }

        /// <summary>
        ///     The check table words.
        /// </summary>
        public void CheckTableWords()
        {
            this.server.Refresh();
            this.server.Databases.Refresh();
            var database = this.server.Databases[DBName];

            var tables = database.Tables;
            database.Tables.Refresh();

            if (!tables.Contains(TableName))
            {
                this.CreateTable(database, TableName);
            }
        }

        /// <summary>
        /// The create table.
        /// </summary>
        /// <param name="db">
        /// The db.
        /// </param>
        /// <param name="name">
        /// The name.
        /// </param>
        private void CreateTable(Database db, string name)
        {
            var tb = new Table(db, name);
            var col2 = new Column(tb, "Word", DataType.NChar(30));

            var col1 = new Column(tb, "ID", DataType.Int) { Identity = true, Nullable = false };

            this.CreateIndex(tb, col1);

            tb.Columns.Add(col1);
            tb.Columns.Add(col2);
            tb.Create();
        }

        /// <summary>
        /// The create index.
        /// </summary>
        /// <param name="tb">
        /// The tb.
        /// </param>
        /// <param name="col1">
        /// The col 1.
        /// </param>
        private void CreateIndex(Table tb, Column col1)
        {
            var idx = new Index(tb, "PK_Words");

            tb.Indexes.Add(idx);

            idx.IndexedColumns.Add(new IndexedColumn(idx, col1.Name));

            idx.IsClustered = true;

            idx.IsUnique = true;

            idx.IndexKeyType = IndexKeyType.DriPrimaryKey;
        }

        /// <summary>
        ///     The truncate.
        /// </summary>
        public void Truncate()
        {
            var table = this.WordsTable;

            table.TruncateData();
        }

        /// <summary>
        ///     Gets the words table.
        /// </summary>
        private Table WordsTable
        {
            get
            {
                var database = this.server.Databases[DBName];
                var tables = database.Tables;

                var table = tables[TableName];
                return table;
            }
        }

        /// <summary>
        /// The bulk insert.
        /// </summary>
        /// <param name="dt">
        /// The dt.
        /// </param>
        public void BulkInsert(DataTable dt)
        {
            var cnn = this.GetSqlConnection();

            var bulkCopy = new SqlBulkCopy(cnn.ConnectionString, SqlBulkCopyOptions.TableLock);
            var sqlBulkInsert = bulkCopy;
            sqlBulkInsert.ColumnMappings.Add(0, 1);
            sqlBulkInsert.DestinationTableName = TableName;

            sqlBulkInsert.WriteToServer(dt);
        }

        /// <summary>
        ///     The get sql connection.
        /// </summary>
        /// <returns>
        ///     The <see cref="SqlConnection" />.
        /// </returns>
        private SqlConnection GetSqlConnection()
        {
            var cb = new SqlConnectionStringBuilder
                         {
                             DataSource = this.server.ConnectionContext.ServerInstance, 
                             InitialCatalog = DBName, 
                             UserID = this.server.ConnectionContext.Login, 
                             Password = this.server.ConnectionContext.Password
                         };
            var cnn = new SqlConnection(cb.ConnectionString);
            return cnn;
        }

        /// <summary>
        ///     The on disconnect.
        /// </summary>
        protected virtual void OnDisconnect()
        {
            var handler = this.Disconnect;
            if (handler != null)
            {
                handler();
            }
        }

        /// <summary>
        /// The read.
        /// </summary>
        /// <param name="sql">
        /// The sql.
        /// </param>
        /// <param name="make">
        /// The make.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<T> Read<T>(string sql, Func<IDataReader, T> make)
        {
            using (var connection = this.CreateConnection())
            {
                using (var command = this.CreateCommand(sql, connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return make(reader);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     The create connection.
        /// </summary>
        /// <returns>
        ///     The <see cref="SqlConnection" />.
        /// </returns>
        private SqlConnection CreateConnection()
        {
            var connection = this.GetSqlConnection();
            connection.Open();
            return connection;
        }

        /// <summary>
        /// The create command.
        /// </summary>
        /// <param name="sql">
        /// The sql.
        /// </param>
        /// <param name="conn">
        /// The conn.
        /// </param>
        /// <returns>
        /// The <see cref="SqlCommand"/>.
        /// </returns>
        private SqlCommand CreateCommand(string sql, SqlConnection conn)
        {
            SqlCommand command = new SqlCommand();
            command.Connection = conn;
            command.CommandText = sql;
            return command;
        }
    }
}